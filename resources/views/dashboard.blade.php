@extends('main')

@section('container')
      <div class="row">
        <div class="pull-right mb-2 ml-2">
            <h4 class="mb-5"><i class="fas fa-user mr-2 mt-4"></i><b>DATA MAHASISWA</b></h4>
                <a href="/add_data">
                    <button class="btn btn-success rounded-pill"><i class="fas fa-plus-square mr-2"></i>Tambah Data</button>
                </a>
        </div>
    </div>
    <div class="row">
        <table class="table table-bordered table-hover mt-2">
            <thead>
                <tr class="bg-light text-blank font-weight-bold">
                    <td>No</td>
                    <td>Nama</td>
                    <td>NIM</td>
                    <td>Kelas</td>
                    <td>Prodi</td>
                    <td>Fakultas</td>
                    <td>Ubah</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($data_mhs as $data)
                <tr>
                    <td>{{ $data->id }}</td>
                    <td>{{ $data->nama_mahasiswa }}</td>
                    <td>{{ $data->nim_mahasiswa}}</td>
                    <td>{{ $data->kelas_mahasiswa }}</td>
                    <td>{{ $data->prodi_mahasiswa }}</td>
                    <td>{{ $data->fakultas_mahasiswa }}</td>
                    <td>
                        <a href="/update/{{ $data->id }}"><i class="fas fa-edit mr-2"></i> </a>
                        <a href="/delete_data/{{ $data->id }}"><i class="fas fa-trash text-danger mr-2"></i> </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
            </li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item">
                <a class="page-link" href="#">Next</a>
            </li>
        </ul>
    </nav>
@endsection