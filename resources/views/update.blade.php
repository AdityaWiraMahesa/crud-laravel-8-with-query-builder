@extends('main')

@section('container')
<br><h5 class="ml-2 mb-3"><b><i class="fas fa-user mr-2"></i>TAMBAH DATA MAHASISWA</b></h5>
<form action="/simpanedit" method="post" role="form">
    {{ csrf_field() }}
    <div class="row">
        <div class="ml-2 mr-5">
            <div class="card-body mt-0 shadow p-3 mb-4 ml-2 mr-2 bg-success rounded"> <b>Note : </b>Mohon mengisi sesuai dengan data mahasiswa sebenarnya</div>
        </div>
        <div class="col-md-5 ml-2">
            <div class="form-grup">
                <input type="hidden" name="id" required value="{{ $data->id }}" class="form-control">
            </div>
            <div class="form-grup">
                <label for="">Nama</label>
                <input type="text" name="nama_mhs" required value="{{ $data->nama_mahasiswa }}" class="form-control">
            </div>
            <div class="form-grup">
                <label for="">NIM</label>
                <input type="text" name="nim_mhs" required value="{{ $data->nim_mahasiswa }}" class="form-control">
            </div>
            <div class="form-grup">
                <label for="">Kelas</label>
                <input type="text" name="kelas_mhs" required value="{{ $data->kelas_mahasiswa }}" class="form-control">
            </div>
            <br>
            <div class="form-grup">
                <button type="submit" class="btn btn-primary rounded-pill"><i class="fas fa-save mr-2"></i>Save</button>
                <button type="reset" class="btn btn-success ml-1 rounded-pill"><i class="fas fa-undo mr-2"></i>Reset</button>
                <button type="submit" class="btn btn-danger ml-1 rounded-pill"><a href="/" class="text-white"><i class="fas fa-arrow-circle-left mr-2"></i>Back</a></button>
            </div>
        </div>
        <div class="col-md-5 ml-2">
            <div class="form-grup">
                <label for="">Program Studi</label>
                <input type="text" name="prodi_mhs" required value="{{ $data->prodi_mahasiswa }}" class="form-control">
            </div>
            <div class="form-grup">
                <label for="">Fakultas</label>
                <input type="text" name="fakultas_mhs" required value="{{ $data->fakultas_mahasiswa }}" class="form-control">
            </div>
        </div>
    </div>
</form>



@endsection