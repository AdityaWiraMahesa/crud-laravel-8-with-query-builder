<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function dashboard(){
        $hasil = DB::table('mahasiswa')->get();
        return view('dashboard', ['data_mhs'=>$hasil]);
    }

    public function prosesSimpan(Request $req){
        $nama_mhs = $req->nama_mhs;
        $nim_mhs = $req->nim_mhs;
        $kelas_mhs = $req->kelas_mhs;
        $prodi_mhs = $req->prodi_mhs;
        $fakultas_mhs = $req->fakultas_mhs;

        DB::table('mahasiswa')->insert(
            [
                'nama_mahasiswa'=>$nama_mhs,
                'nim_mahasiswa'=>$nim_mhs,
                'kelas_mahasiswa'=>$kelas_mhs,
                'prodi_mahasiswa'=>$prodi_mhs,
                'fakultas_mahasiswa'=>$fakultas_mhs
            ]
        );

        return redirect ('/');
    }

    public function Edit($id){
        $data = DB::table('mahasiswa')->where('id', $id)->first();
        return view('update', compact('data'));
    }

    public function SimpanEdit(Request $req){
        $nama_mhs = $req->nama_mhs;
        $nim_mhs = $req->nim_mhs;
        $kelas_mhs = $req->kelas_mhs;
        $prodi_mhs = $req->prodi_mhs;
        $fakultas_mhs = $req->fakultas_mhs;

        DB::table('mahasiswa')->where('id',$req['id'])->update(
            [
                'nama_mahasiswa'=>$nama_mhs,
                'nim_mahasiswa'=>$nim_mhs,
                'kelas_mahasiswa'=>$kelas_mhs,
                'prodi_mahasiswa'=>$prodi_mhs,
                'fakultas_mahasiswa'=>$fakultas_mhs
            ]
        );

        return redirect ('/');
    }

    public function Delete($id){
        DB::table('mahasiswa')->where('id', $id)->delete();
        return redirect ('/');
    }
}
