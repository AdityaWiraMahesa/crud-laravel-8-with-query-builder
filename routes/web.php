<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [App\Http\Controllers\DashboardController::class,'dashboard']);
Route::view('/add_data', 'Add');
Route::post('/simpan', [App\Http\Controllers\DashboardController::class,'prosesSimpan']);
Route::get('/update/{id}', [App\Http\Controllers\DashboardController::class,'Edit']);
Route::post('/simpanedit', [App\Http\Controllers\DashboardController::class,'SimpanEdit']);
Route::get('/delete_data/{id}', [App\Http\Controllers\DashboardController::class,'Delete']);